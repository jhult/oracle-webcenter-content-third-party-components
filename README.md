# README #
This is a backup repository to host WebCenter Content components from third parties. It exists for two main reasons:

* One stop shop for all components
* As a backup in case the original source disappears

## Credit ##
All credit for the component goes to the original source (which I try to reference in the commit comments).

## Warranty ##
These components are provided without any warranty by me. If you have an issue, please contact the original source.

## Contribution guidelines ##
If you want to contribute a component, feel free to submit a pull request (please also mention the original source in the commit comment).